import Vue from 'vue';
import App from './App.vue';
import VueClipboard from 'vue-clipboard2';
import Notifications from 'vue-notification';
import {
  FontAwesomeIcon,
} from '@fortawesome/vue-fontawesome';
import {
  library,
} from '@fortawesome/fontawesome-svg-core';

import {
  faBold,
  faItalic,
  faStrikethrough,
  faUnderline,
  faCode,
  faParagraph,
  faListUl,
  faListOl,
} from '@fortawesome/free-solid-svg-icons';


VueClipboard.config.autoSetContainer = true; // add this line
Vue.use(VueClipboard);
Vue.use(Notifications);

library.add({
  faBold,
  faItalic,
  faStrikethrough,
  faUnderline,
  faCode,
  faParagraph,
  faListUl,
  faListOl,
});

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
